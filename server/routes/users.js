var express = require("express");
var router = express.Router();

// We import a list of users from "server/data/user-db.js".
// Normally you would use a database like mySQL or Postresql,
// but let's keep things simple here.
var users = require("../data/user-db");

/* GET users listing. */
router.get("/", function(req, res) {
  // You can send the entire user list back.
  // Express will take care of telling the browser that this is "just data" and not HTML.
  // If you're interested in how this works, go here: https://stackoverflow.com/a/3828381/932315
  res.send(users);
});

/* GET a specific user */
router.get("/:userId", function(req, res) {
  // This route listenes to requests like:
  // http://localhost:3000/users/2
  // The userId of the request is stored in the req.params object.
  // Be careful! Every parameter is stored as a string.
  // We need to convert it to an integer so we can compare it to a user.id.
  var userId = parseInt(req.params.userId);

  // users.find() takes a function and returns a user if the function returns true.
  var result = users.find(user => user.id === userId);

  // You could write the same code like this:
  //
  // function doesMatch(user){ return user.id === userId }
  // var result = users.find(doesMatch);

  res.send(result); // Send the found user back as JSON result.
});

module.exports = router;

// And that's basically it!
// You can take a final look at "/bin/www" and then continue with the react-example. :)
