# a-little-frontend-help

Explanations and resources for front-end devs.

## What's in here

- **Classic:** A classic HTML page with some basic Javascript talking to the server.
- **Server:** A node.js based example server
- **React-SPA:** An example project in react that talks to the server.

## How to use

Start with the README in the `Server` directory, it will guide you through the rest of the demo.
