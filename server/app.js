/*
  Now that you've installed your dependencies, you can use them in your code.
  You do this by "importing" the libraries you need.

  This is similar to how you would use other code in languages like
  python, C# or Java.

  You can import other code in various ways:
*/

// import cookieParser from "cookie-parser";

// 1. Importing the default code from an external library
// This is the simplest one:
// It will search your node_modules folder for a subfolder called "express".
// If it exists, it will load the code in "express/index.js" and put it into the createError variable.
var express = require("express");
var logger = require("morgan");
var path = require("path");
//
// 2. If you want to import your own code, you use the relative path to the file.
// indexRouter will be imported from "server/routes/index.js".
var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");

// This function call creates the server for you.
// You can now configure your server by calling various functions of "app".
var app = express();

/*
 Imagine your server as a pipeline.
 If a request comes in, it will flow through one function after another.
 Here we register these functions in order.

 Request => Server
            -> logs the request
            -> request will be parsed
              -> If it's a JSON request, the express.json() function will handle it
              -> otherwise the express.urlencoded function tries again.
            -> Does the request URL match any registered route?
              -> http://my-server.com/ => Returns the "indexRoute"
              -> http://my-server.com/users => Returns the "usersRoute"
*/

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// app.use(cookieParser());

// Prepares express to load files from the public folder, e.g. "server/public/index.html"
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/users", usersRouter);

// Javascript modules (a js module is a file) can "export" code, so other modules can use it.
// Your configured server is imported in the file: "server/bin/www". Have a look :)
module.exports = app;

// Open routes/index.js next!
