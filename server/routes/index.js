var express = require("express");
var router = express.Router();

/* GET home page. */
router.get("/", function(req, res, next) {
  // res.render will return a HTML view, that your browser can display.
  res.render(
    "index" // The name of the view you want to return. This will resolve to "server/public/index.html"
  );
});

module.exports = router;

// Next step: users.js
