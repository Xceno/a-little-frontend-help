var userList = [
  {
    id: 1,
    name: "John Doe",
    job: "Backend Developer",
    likes: ["eating", "sleeping", "떡볶이"]
  },
  {
    id: 2,
    name: "Jane Doe",
    job: "Frontend Developer",
    likes: ["따아", "마파두부", "김치볶음밥"]
  }
];

module.exports = userList;
